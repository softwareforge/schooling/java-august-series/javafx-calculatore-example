package pl.softwareforge.view;

import com.jfoenix.controls.JFXTextArea;
import javafx.fxml.FXML;
import javafx.scene.input.MouseEvent;

import com.jfoenix.controls.JFXButton;
import pl.softwareforge.logic.observer.EventListener;
import pl.softwareforge.model.CalculationResult;
import pl.softwareforge.viewmodel.MainPaneViewModel;

import java.util.LinkedList;

public class MainPaneView
        implements EventListener
{
    @FXML private JFXTextArea resultTextArea;

    private MainPaneViewModel mainPaneViewModel;

    public MainPaneView(MainPaneViewModel viewModel)
    {
        mainPaneViewModel = viewModel;
        mainPaneViewModel.getObservableResults().register(this);
    }

    @FXML private void onButtonMouseClick(MouseEvent mouseEvent)
    {
        mainPaneViewModel.addNumberInput(((JFXButton) mouseEvent.getSource()).getText());
        setResultTextArea();
    }

    @FXML private void onEqualsButtonMouseClick(MouseEvent mouseEvent)
    {
        mainPaneViewModel.addNumberInput(((JFXButton) mouseEvent.getSource()).getText());
    }

    @Override public void onUpdate(Object context)
    {
        var result = ((LinkedList<CalculationResult>) context).getLast();
        resultTextArea.setText(result.toString());
    }

    private void setResultTextArea()
    {
        var out = "";
        var firstInput = mainPaneViewModel.getFirstInput();
        var secondInput = mainPaneViewModel.getSecondInput();
        var operator = mainPaneViewModel.getOperator();

        if( !firstInput.isEmpty() )
        {
            out = out + firstInput;

            if ( operator != null )
            {
                out = out + " " + operator.toString();

                if( !secondInput.isEmpty() )
                {
                    out = out + " " + secondInput;
                }
            }
        }
        
        resultTextArea.setText(out);
    }
}