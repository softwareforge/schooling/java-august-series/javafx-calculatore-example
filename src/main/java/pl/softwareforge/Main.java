package pl.softwareforge;

public class Main
{
    public static void main(String[] args)
    {
        var calculator = new Calculator();
        calculator.run(args);
    }
}