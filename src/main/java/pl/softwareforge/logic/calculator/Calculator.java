package pl.softwareforge.logic.calculator;

import pl.softwareforge.logic.calculator.operation.*;
import pl.softwareforge.model.CalculationResult;

import java.util.HashMap;

public class Calculator
{
    private final HashMap<Character, Operation> knownOperationMap = new HashMap<>();

    public Calculator()
    {
        var addOperation = new AddOperation();
        var subtractOperation = new SubtractOperation();
        var multiplyOperation = new MultiplyOperation();
        var divideOperation = new DivideOperation();

        knownOperationMap.put(addOperation.getSymbol(), addOperation);
        knownOperationMap.put(subtractOperation.getSymbol(), subtractOperation);
        knownOperationMap.put(multiplyOperation.getSymbol(), multiplyOperation);
        knownOperationMap.put(divideOperation.getSymbol(), divideOperation);
    }

    public CalculationResult calculate(Float firstValue,
                                       Float secondValue,
                                       Character operator)
    {
        return knownOperationMap.get(operator).execute(firstValue, secondValue);
    }
}
