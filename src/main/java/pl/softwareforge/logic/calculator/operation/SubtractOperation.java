package pl.softwareforge.logic.calculator.operation;

import pl.softwareforge.model.CalculationResult;

public class SubtractOperation
        implements Operation
{
    @Override public CalculationResult execute(Float first, Float second)
    {
        return new CalculationResult(first, second, getSymbol(), first - second);
    }
    @Override public Character getSymbol()
    {
        return '-';
    }
}
