package pl.softwareforge.logic.calculator.operation;

import pl.softwareforge.model.CalculationResult;

public interface Operation
{
    CalculationResult execute(Float firstValue, Float secondValue);
    Character getSymbol();
}
