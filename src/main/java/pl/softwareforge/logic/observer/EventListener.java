package pl.softwareforge.logic.observer;

public interface EventListener
{
    void onUpdate(Object context);
}
