package pl.softwareforge.logic.observer;

import java.util.Collection;
import java.util.LinkedList;

public class Observable<T>
{
    private T observable;
    private Collection<EventListener> eventListeners = new LinkedList<>();

    public Observable(T observable)
    {
        this.observable = observable;
    }

    public void register(EventListener listener) {eventListeners.add(listener);}
    public T get() {return observable;}

    public void sendUpdate()
    {
        for (var listener : eventListeners)
        {
            listener.onUpdate(observable);
        }
    }
}
