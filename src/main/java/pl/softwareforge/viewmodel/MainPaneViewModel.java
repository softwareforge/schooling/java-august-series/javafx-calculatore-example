package pl.softwareforge.viewmodel;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import pl.softwareforge.logic.calculator.Calculator;
import pl.softwareforge.logic.observer.Observable;
import pl.softwareforge.model.CalculationResult;

import java.util.LinkedList;
import java.util.List;

public class MainPaneViewModel
{
    private Observable<List<CalculationResult>> observableCalculationResultList = new Observable<>(new LinkedList<>());
    private final Calculator calculator = new Calculator();

    private String firstInput = "";
    private String secondInput = "";
    private Character operator = null;

    public String getFirstInput() {return firstInput;}
    public String getSecondInput() {return secondInput;}
    public Character getOperator() {return operator;}

    public Observable<List<CalculationResult>> getObservableResults()
    {
        return observableCalculationResultList;
    }
    public void addNumberInput(String input)
    {
        try
        {
            Float.parseFloat(input);
            if (operator == null)
            {
                firstInput = firstInput + input;
            }
            else
            {
                secondInput = secondInput + input;
            }
        }
        catch (NumberFormatException ex)
        {
            var readOperator = input.charAt(0);
            if (readOperator == '=')
            {
                if (operator == null) {return;}

                var result =
                        calculator.calculate(
                                Float.parseFloat(firstInput),
                                Float.parseFloat(secondInput),
                                operator
                                            );

                firstInput = result.getResult().toString();
                secondInput = "";
                operator = null;

                observableCalculationResultList.get().add(result);
                observableCalculationResultList.sendUpdate();
                return;
            }
            else if (readOperator == 'C')
            {
                firstInput = "";
                secondInput = "";
                operator = null;
            }
            else
            {
                operator = readOperator;
            }
        }
    }
}
