package pl.softwareforge.model;

public class CalculationResult
{
    private Float firstNumber;
    private Float secondNumber;
    private Character operator;
    private Float result;

    public CalculationResult(Float firstNumber,
                             Float secondNumber,
                             Character operator,
                             Float result)
    {
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
        this.operator = operator;
        this.result = result;
    }

    public Float getResult() { return result; }
    @Override public String toString()
    {
        return firstNumber + " " + operator + " " + secondNumber + " = " + result;
    }
}
