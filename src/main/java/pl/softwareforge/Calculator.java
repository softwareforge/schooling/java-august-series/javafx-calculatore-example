package pl.softwareforge;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pl.softwareforge.view.MainPaneView;
import pl.softwareforge.viewmodel.MainPaneViewModel;

import java.io.IOException;

public class Calculator
        extends Application
{
    @Override
    public void start(Stage primaryStage) throws IOException
    {
        var fxmlLoader = new FXMLLoader(getClass().getResource("/view/MainPane.fxml"));
        fxmlLoader.setController(new MainPaneView(new MainPaneViewModel()));

        try
        {
            Parent root = fxmlLoader.load();
            var scene = new Scene(root);
            scene.getStylesheets().add("/style/style.css");
            primaryStage.setScene(scene);
            primaryStage.show();
        }
        catch (IOException exception)
        {
            exception.printStackTrace();
        }
    }

    public void run(String[] args)
    {
        launch(args);
    }
}
