package viewmodel;

import org.junit.jupiter.api.*;
import pl.softwareforge.model.CalculationResult;
import pl.softwareforge.viewmodel.MainPaneViewModel;

import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.*;

public class MainPaneViewModelTest
{
    @DisplayName("Should concat string")
    @Test
    public void TestIfInputWorksCorrectly()
    {
        var sut = new MainPaneViewModel();

        sut.addNumberInput("3");
        assertEquals("3", sut.getFirstInput());

        sut.addNumberInput("4");
        assertEquals("34", sut.getFirstInput());
    }

    @DisplayName("Should concat string for both numbers after +")
    @Test
    void TestIfBothInputsWorkCorrectly()
    {
        var sut = new MainPaneViewModel();

        sut.addNumberInput("3");
        assertEquals("3", sut.getFirstInput());
        assertEquals("", sut.getSecondInput());

        sut.addNumberInput("4");
        assertEquals("34", sut.getFirstInput());
        assertEquals("", sut.getSecondInput());

        sut.addNumberInput("+");
        assertEquals("34", sut.getFirstInput());
        assertEquals("", sut.getSecondInput());

        sut.addNumberInput("4");
        assertEquals("34", sut.getFirstInput());
        assertEquals("4", sut.getSecondInput());

        sut.addNumberInput("4");
        assertEquals("34", sut.getFirstInput());
        assertEquals("44", sut.getSecondInput());
    }

    @DisplayName("Check if result is being returned correctly")
    @Test
    void TestIfResultWorkCorrectly()
    {
        var sut = new MainPaneViewModel();


        sut.addNumberInput("3");
        sut.addNumberInput("3");
        sut.addNumberInput("3");
        sut.addNumberInput("+");
        sut.addNumberInput("4");
        sut.addNumberInput("6");
        sut.addNumberInput("7");
        sut.addNumberInput("=");

        var result = ((LinkedList<CalculationResult>) sut.getObservableResults().get()).getLast();

        assertEquals(result.getResult(), 333 + 467);

    }

    @DisplayName("Check addition")
    @Test
    void TestIfAdditionWorksCorrectly()
    {
        var sut = new MainPaneViewModel();


        assertDoesNotThrow(() -> sut.addNumberInput("3"));
        assertDoesNotThrow(() -> sut.addNumberInput("3"));
        assertDoesNotThrow(() -> sut.addNumberInput("3"));
        assertDoesNotThrow(() -> sut.addNumberInput("+"));
        assertDoesNotThrow(() -> sut.addNumberInput("4"));
        assertDoesNotThrow(() -> sut.addNumberInput("6"));
        assertDoesNotThrow(() -> sut.addNumberInput("7"));
        assertDoesNotThrow(() -> sut.addNumberInput("="));

        var result = ((LinkedList<CalculationResult>) sut.getObservableResults().get()).getLast();

        assertEquals(result.getResult(), 333 + 467);

    }

    @DisplayName("Check subtraction")
    @Test
    void TestIfSubtractionWorksCorrectly()
    {
        var sut = new MainPaneViewModel();


        assertDoesNotThrow(() -> sut.addNumberInput("3"));
        assertDoesNotThrow(() -> sut.addNumberInput("3"));
        assertDoesNotThrow(() -> sut.addNumberInput("3"));
        assertDoesNotThrow(() -> sut.addNumberInput("-"));
        assertDoesNotThrow(() -> sut.addNumberInput("4"));
        assertDoesNotThrow(() -> sut.addNumberInput("6"));
        assertDoesNotThrow(() -> sut.addNumberInput("7"));
        assertDoesNotThrow(() -> sut.addNumberInput("="));

        var result = ((LinkedList<CalculationResult>) sut.getObservableResults().get()).getLast();

        assertEquals(result.getResult(), 333 - 467);

    }

    @DisplayName("Check multiplication")
    @Test
    void TestIfMultiplicationWorksCorrectly()
    {
        var sut = new MainPaneViewModel();


        assertDoesNotThrow(() -> sut.addNumberInput("3"));
        assertDoesNotThrow(() -> sut.addNumberInput("3"));
        assertDoesNotThrow(() -> sut.addNumberInput("3"));
        assertDoesNotThrow(() -> sut.addNumberInput("*"));
        assertDoesNotThrow(() -> sut.addNumberInput("4"));
        assertDoesNotThrow(() -> sut.addNumberInput("6"));
        assertDoesNotThrow(() -> sut.addNumberInput("7"));
        assertDoesNotThrow(() -> sut.addNumberInput("="));

        var result = ((LinkedList<CalculationResult>) sut.getObservableResults().get()).getLast();

        assertEquals(result.getResult(), 333 * 467);

    }

    @DisplayName("Check division")
    @Test
    void TestIfDivisionWorksCorrectly()
    {
        var sut = new MainPaneViewModel();


        assertDoesNotThrow(() -> sut.addNumberInput("3"));
        assertDoesNotThrow(() -> sut.addNumberInput("3"));
        assertDoesNotThrow(() -> sut.addNumberInput("3"));
        assertDoesNotThrow(() -> sut.addNumberInput("/"));
        assertDoesNotThrow(() -> sut.addNumberInput("4"));
        assertDoesNotThrow(() -> sut.addNumberInput("6"));
        assertDoesNotThrow(() -> sut.addNumberInput("7"));
        assertDoesNotThrow(() -> sut.addNumberInput("="));

        var result = ((LinkedList<CalculationResult>) sut.getObservableResults().get()).getLast();

        assertEquals(result.getResult(), 333f / 467f);

    }

}
